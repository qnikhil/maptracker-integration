# Maptracker integration #

This repository contains code required to integrate various platforms with the Maptracker.

## Setup
To get started with the maptracker integration code, clone the repo and install the dependencies (you most likely have them already).
If you like, you can run them in a python virtual environment as below (recommended).

```
git clone git@bitbucket.org:ariell/maptracker-integration.git
cd maptracker-integration
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

### Running the demo module
```
python platform_scripts/demo_platform_apipost.py
```
This assumes that you have an instance of maptracker server running on the same machine at ```http://localhost:8080```.
You can change the default server by passing cli arguments. Run with the ```-h``` flag for more info.
This script will perform HTTP POST requests sending updates of the platform data, missions and demo log messages to the maptracker API.
If there is no maptracker server running at the address provided, or it cannot connect for some reason, it will print out debug information, so you can still test your script in isolation.

#### Running the demo module with an online preview
```
python platform_scripts/demo_platform_apipost.py -u http://144.6.225.82
```
This will post the data to a maptracker server running at: [http://144.6.225.82:8080](http://144.6.225.82:8080).
Click that link to see the maptracker instance. You can test your own modules using the online server.

## Integration Guide
The best starting point is to take a look at the demo module: ```platform_scripts/demo_platform_apipost.py```.
From there, it should be pretty self explanatory how to integrate your own platform.

### Boilerplate example

The basic construct for a script to integrate your nav data is as follows:
```
from platform_tools.platformdata import APIPlatform, get_args

class MyNewPlatform(APIPlatform):                     # Derived class with convenience methods for maptracker API
    def start_threads(self):
        platform_key = "key_for_my_new_platform"      # Must be unique
        platform_data = ...                                    # See below for example data structure
        self.update_platform_data(platform_key, platform_data) # Post data to API

if __name__ == "__main__":

    # get cli args
    args = get_args()   # URL and port for maptracker server are set through command line args

    # Instantiate class
    platform = MyNewPlatform(maptracker_url=args["url"], maptracker_port=args["port"])

    # Start data threads
    platform.start_threads()

    # keep main thread alive
    platform.serve_forever()
```

Where the ```MyNewPlatform.start_threads``` method would start a thread that acquires your platform's data stream.
For example, if your platform is based on LCM, it would subscribe to the appropriate channel and then send through any
messages that are received. Checkout the ACFR example in: ```plaform_scripts/acfr_lcm_apipost.py``` for more info.


### More details
Make a copy of the ```demo_platform_apipost.py``` and rename it prefixed with your institution, eg: the ACFR module is:
```acfr_lcm_apipost.py```. Place your code into the ```platform_scripts``` directory, where it will have access to the
```platform_tools```module which provides a number of convenient methods for integration with the maptracker API.
Eg the ```platform_tools.platformdata.APIPlatform``` class has the the following methods:

* ```APIPlatform.update_platform_data(platform_key, platform_data)```: post platform data to API.
    * ```platform_key``` *string*: is a unique key for each platform in the API.
    * ```platform_data``` *dict, platformdata*: is a dictionary containing the platform data key-value pairs defined below.
* ```APIPlatform.update_platform_mission(platform_key, mission_data)```: post mission geojson data to API.
    * ```platform_key``` *as above*
    * ```mission_data``` *object, geojson*: is a geojson object as output by the python-geojson module (available through pip).
* ```APIPlatform.log_platform_message(platform_key, msg_type, msg)```: post a log message to the API.
    * ```platform_key``` *as above*
    * ```msg_type``` *string*: the type for the log message to send to the UI. Can be one of ```success|warning|danger|info```.
    * ```msg``` *string*: the log message to send to the UI.
* ```APIPlatform.serve_forever()```: keep the main thread alive. Using this convenience method, will make it possible to start a webserver for this platform to receive commands.


### Sending log messages
You can pass through log messages to the UI from your code.
This is useful for sending through messages from your module through to the GUI.
The messages will show up in the maptracker GUI in the logger console (if the layer is configured).

Send a message using a platform instance:
```
platform.log_platform_message(platform_key, msg_type, msg)
```
Send a message using from a method inside a platform class (eg: MyNewPlatform):
```
self.log_platform_message(platform_key, msg_type, msg)
```
To post a message without a platform instance, you can import the post_data function:
```
from platform_tools.platformdata import post_data
post_data(platform_key, {"msg": msg, "type": msg_type}, data_type="log")
```

### Example Data Structures
#### platform_data
This is the example from the demo module to generate random data around an origin
```
#!python
platform_data = {
    # required: lat, lon
    # optional, but required for dash display: heading, roll, pitch
    # optional, speed, uncertainty, alt, depth
    # all values must be floats
    "pose": {
        "lat": float(-33.83989521),         # REQUIRED, float, decimal degrees
        "lon": float(151.25314325),         # REQUIRED, float, decimal degrees
        "alt": float(2),                    # float, m
        "depth": float(123.1),              # float, m
        "heading": float(87),               # float, degrees, range: 0:360
        "pitch": float(10),                 # float, degrees, range: -180:180
        "roll": float(0),                   # float, degrees, range: -180:180
        "speed": float(0.5),                # float, m/s
        "uncertainty": float(5)             # float, m
    },

    # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
    # optional, but required for dash display: bat
    # The keys can be anything, but keep them short for display.
    # The values can be anything, with the exception of bat, which must be an int
    "stat": {
        "bat": randrange(0, 100),  # int, %, range: 0:100
        "state": "OK",
        "foo": "bar",
        "something": "else"
    },

    # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
    # The keys can be anything, but keep them short for display.
    # The values need to be int 0/1, with 1=alert, 0=ok.
    "alert": {
        "coms": 0,
        "leak": 1,      # example leak detected!
        "power": 0,
        "state": 0,
        "status": 0
    }
}
```

#### mission_data
The example below uses the ```geojson``` python module (available through ```pip install geojson```)
```
#!python
from geojson import Point, FeatureCollection, LineString, Feature

origin = [-33.8405831822, 151.2548891302]   # LAT-LON or demo origin
origin = origin[::-1]  # NOTE: GeoJSON expects LON-LAT order (i.e. x-y) which is different to lat-lon convention

# Generate random points around the origin
lnglats = [(origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500),
           (origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500),
           (origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500),
           (origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500)]

mission_data = FeatureCollection([
    Feature(geometry=Point(origin), properties={
        # Styling options
        "options": {"radius": 6, "fillColor": "yellow", "color": "black", "weight": 2, "opacity": 0.5, "fillOpacity": 0.5},
        # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
        "info": {"Type": "Point: Origin", "Name": "Random Mission"}
    }),
    Feature(geometry=Point(lnglats[0]), properties={
        # Styling options
        "options": {"radius": 6, "fillColor": "yellow", "color": "white", "weight": 1, "opacity": 1, "fillOpacity": 0.8},
        # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
        "info": {"Type": "Point: START", "Name": "Random Mission"}
    }),
    Feature(geometry=Point(lnglats[-1]), properties={
        # Styling options
        "options": {"radius": 6, "fillColor": "yellow", "color": "black", "weight": 1, "opacity": 1, "fillOpacity": 0.8},
        # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
        "info": {"Type": "Point: END", "Name": "Random Mission"}
    }),
    Feature(geometry=LineString(lnglats), properties={
        # Styling options
        "options": {"color": "yellow", "weight": 3, "opacity": 0.5, "dashArray": "5,5"},
        # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
        "info": {"Type": "LineString: Path", "Name": "Random Mission", "Duration": "5 Hours"}
    })
    # NOTE: Can also include a Polygon features in a similar way
])
```
