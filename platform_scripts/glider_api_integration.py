from time import sleep
from platform_tools.platformdata import APIPlatform, get_args
import requests


platform_key = 'whoi.slocum'
GLIDER_GET_URL = 'http://10.23.14.113:8080/kirk/state/glider-250'

class SlocumGliderPlatform(APIPlatform):
    def start_threads(self, platform_key):
        last_checked_time = ''
        last_known_pose = {
            'x': 0,
            'y': 0,
        }
        while True:
            sleep(1)
            try:
                # perform POST and get back objects
                r = requests.get(GLIDER_GET_URL)
                if r.status_code != 200:
                    error_msg = 'Request to glider state failed with status %d' % r.status_code
                    self.log_platform_message(platform_key, 'warning', error_msg)
                    print(error_msg)
                    continue
                data = r.json()

                is_simulating = data.get('simulatingP', False)
                if is_simulating:
                    print('In simulation mode...')
                    continue

                x = data['pose']['point']['x']
                y = data['pose']['point']['y']
                last_heard = data.get('lastHeardFrom', None)
                if (last_heard is not None and last_heard != last_checked_time) or last_known_pose['x'] != x or last_known_pose['y'] != y:
                    mission_data = {
                        'pose': {
                            'lat': y,
                            'lon': x,
                            'uncertainty': round(data['uncertainty'], 1),
                            'speed': float(0.4),
                        },
                        'stat': {},
                        'alert': {},
                    }
                    last_known_pose['x'] = x
                    last_known_pose['y'] = y
                    last_checked_time = last_heard
                    self.update_platform_data(platform_key, mission_data)
            except Exception as e:
                err_str = 'Issue with fetching glider data: %s' %repr(e)
                print(err_str)
                self.log_platform_message(platform_key, 'warning', err_str)



if __name__ == "__main__":

    # get cli args
    args = get_args()

    # Instantiate class
    platform = SlocumGliderPlatform(maptracker_url=args["url"], maptracker_port=args["port"])

    # Start data threads
    platform.start_threads(platform_key)

    # keep main thread alive
    platform.serve_forever()
