import argparse
import requests
from time import sleep
from flask import Flask, request, render_template, jsonify
import socket


default_maptracker_url = "http://localhost"
default_maptracker_port = 8080
default_command_server_port = None

# Get my IP
my_ip = (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["0.0.0.0"])[0]

class APIPlatform:
    maptracker_url = default_maptracker_url
    maptracker_port = default_maptracker_port
    command_server_port = default_command_server_port
    app = Flask(__name__, template_folder="../../templates")   # mini flask webserver

    def __init__(self, maptracker_url=default_maptracker_url, maptracker_port=default_maptracker_port, command_server_port=default_command_server_port):
        self.maptracker_url = maptracker_url
        self.maptracker_port = maptracker_port
        self.command_server_port = command_server_port
        # if self.command_server_port is not None:
        self.app.add_url_rule('/execute/<platform>', 'execute_endpoint', view_func=self.command_endpoint, methods=["GET", "POST"])  # add endpoint for command

    def post_data(self, platform_key, data, data_type="platform"):
        return post_data(platform_key, data, data_type=data_type, maptracker_url=self.maptracker_url, maptracker_port=self.maptracker_port)

    def update_platform_data(self, platform_key, data):
        response = self.post_data(platform_key, data, data_type="platform")
        return response, data

    def update_platform_mission(self, platform_key, mission_data):
        response = self.post_data(platform_key, mission_data, data_type="mission")
        return response, mission_data

    def log_platform_message(self, platform_key, msg_type, msg):
        msg_data = {"msg": msg, "type": msg_type}
        response = self.post_data(platform_key, msg_data, data_type="log")
        return response, msg_data

    def execute_command(self, platform, args):
        """NOT IMPLEMENTED: OVERRIDE THIS METHOD IN YOUR CLASS TO DO SOMETHING USEFUL"""
        print "*** DATA RECEIVED:\n Platform: {}, Args: {}".format(platform, args)
        raise NotImplementedError("NotImplemented: no 'execute_command(self, platform, args)' method found in your class")
        return "Return message after execution... If I could get here, I would return this message to the UI, but I can't..."

    def command_endpoint(self, platform):
        if request.method == 'POST':
            try:
                msg = self.execute_command(platform, request.form.to_dict())
                print "*** SUCCESS! Executed command: Platform: {}, Data: {}".format(platform, request.form.to_dict())
                return jsonify({"response": "success", "platform": platform, "msg": msg}), 200
            except Exception as e:
                print "*** ERROR! Command failed for platform: {} - {}".format(platform, e)
                return jsonify({"response": "error", "platform": platform, "msg": str(e)}), 500
        else:
            args = request.args.to_dict()
            template = request.args.get("template", "forms/send-command.html")
            return render_template(template, platform=platform, args=args)

    def serve_forever(self):
        if self.command_server_port is not None:    # start command server
            print "\n*** TO SEND COMMANDS: set platform config file parameter in maptracker to:"
            print '"commandurl": "send_command/platformdata.api.Platform/test?ip={}&port={}&template=forms/send-command.html"\n'.format(my_ip, self.command_server_port)
            print "*** Starting command server:"
            self.app.run(host='0.0.0.0', port=self.command_server_port)
        else:                                       # just create loop to keep alive
            while True:
                sleep(1)


def post_data(platform_name, data, data_type="platform", maptracker_url="http://localhost", maptracker_port=8080):
    """
    Perform http post to maptracker API
    :param
    """
    resp = None
    try:
        resp = requests.post("{}:{}/data/platformdata.api.Platform/{}?data_type={}".format(
            maptracker_url, maptracker_port, platform_name, data_type), json=data
        )
        # Debug print
        print ("Data type: {data_type}, Key: {platform}, Message: {resp[msg]} ({resp[response]},{code}))".format(
            resp=resp.json(), code=resp.status_code, data_type=data_type, platform=platform_name)
        )
    except requests.exceptions.ConnectionError as ce:
        print ("*** ERROR: unable to connect to maptracker server: {}:{}".format(maptracker_url, maptracker_port))
        print ("*** PRINTING OFFLINE DEBUG INFO:")
        print ("    PLATFORM: {}\n    DATATYPE: {}\n    DATA: {}".format(platform_name, data_type, data))
    return resp


def get_args():
    # Parse input args
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", default=default_maptracker_port, type=int, help="Port for maptracker server")
    parser.add_argument("-u", "--url", default=default_maptracker_url, type=str, help="URL for maptracker server (format: http://url)")
    parser.add_argument("-c", "--command_port", default=default_command_server_port, type=int, help="Port for local command server. If set, server is started")
    args = parser.parse_args()

    return {"url": args.url, "port": args.port, "command_port": args.command_port}


def get_validated(data, key, val_convert=None, val_type=None, val_min=None, val_max=None, val_inlist=None, required=True):
    if key in data:
        if required and data[key] == "":
            raise ValueError("'{}' cannot be empty.".format(key))
        if val_type is not None:
            if not isinstance(data[key], val_type):
                raise ValueError("'{}' must be {}.".format(key, val_type))
        if val_min is not None:
            if data[key] < val_min:
                raise ValueError("'{}' can not be less than {}.".format(key, val_min))
        if val_max is not None:
            if data[key] > val_max:
                raise ValueError("'{}' can not be more than {}.".format(key, val_max))
        if val_inlist is not None:
            if data[key] not in val_inlist:
                raise ValueError("'{}' needs to be one of {}.".format(key, val_inlist))
    else:
        if required:
            raise ValueError("'{}' is missing.".format(key))

    return val_convert(data[key]) if val_convert is not None else data[key]