from random import random, randint, randrange
from geojson import Point, FeatureCollection, LineString, Feature


def get_fake_platform_data(origin):
    data = {
        # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
        # The keys can be anything, but keep them short for display.
        # The values need to be int 0/1, with 1=on, 0=off.
        "alert": {
            "coms": randint(0, 1),
            "leak": randint(0, 1),
            "power": randint(0, 1),
            "state": randint(0, 1),
            "status": randint(0, 1)
        },

        # required: lat, lon, heading
        # optional, but required for dash display: roll, pitch, bat
        # optional, speed, uncertainty, alt, depth
        # all values must be floats
        "pose": {
            "lat": origin[0] + randrange(-100, 100) / 200000.0,  # float, decimal degrees
            "lon": origin[1] + randrange(-100, 100) / 200000.0,  # float, decimal degrees
            "alt": float(randrange(0, 10)),  # float, m
            "depth": float(randrange(0, 10)),  # float, m
            "heading": float(randrange(0, 360)),  # float, degrees, range: 0:360
            "pitch": float(randrange(-90, 90)),  # float, degrees, range: -180:180
            "roll": float(randrange(-90, 90)),  # float, degrees, range: -180:180
            "speed": 0.5,  # float, m/s
            "uncertainty": float(randrange(0, 10))  # float, m
        },

        # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
        # optional, but required for dash display: bat
        # The keys can be anything, but keep them short for display.
        # The values can be anything, with the exception of bat, which must be an int
        "stat": {
            "bat": randrange(0, 100),  # int, %, range: 0:100
            "state": "OK",
            "foo": "bar",
            "something": "else"
        }
    }
    return data


def get_fake_mission(origin):
    origin = origin[::-1]  # NOTE: GeoJSON has different ordering of LAT/LONs

    # Generate random points around the origin
    lnglats = [(origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500),
               (origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500),
               (origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500),
               (origin[0] + (random() - 0.5) / 500, origin[1] + (random() - 0.5) / 500)]

    mission = FeatureCollection([
        Feature(geometry=Point(origin), properties={
            # Styling options
            "options": {"radius": 6, "fillColor": "yellow", "color": "black", "weight": 2, "opacity": 0.5,
                        "fillOpacity": 0.5},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "Point: Origin", "Name": "Random Mission"}
        }),
        Feature(geometry=Point(lnglats[0]), properties={
            # Styling options
            "options": {"radius": 6, "fillColor": "yellow", "color": "white", "weight": 1, "opacity": 1,
                        "fillOpacity": 0.8},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "Point: START", "Name": "Random Mission"}
        }),
        Feature(geometry=Point(lnglats[-1]), properties={
            # Styling options
            "options": {"radius": 6, "fillColor": "yellow", "color": "black", "weight": 1, "opacity": 1,
                        "fillOpacity": 0.8},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "Point: END", "Name": "Random Mission"}
        }),
        Feature(geometry=LineString(lnglats), properties={
            # Styling options
            "options": {"color": "yellow", "weight": 3, "opacity": 0.5, "dashArray": "5,5"},
            # Info fields are fully customisable. Key-value pairs are displayed in a popover when clicked
            "info": {"Type": "LineString: Path", "Name": "Random Mission", "Duration": "5 Hours"}
        })
        # NOTE: Can also include a Polygon features in a similar way
    ])

    return mission


def get_fake_log_message():
    # content for random messages
    adj = ("dumb", "clueless", "old", "ridiculous", "crazy")
    nouns = ("KAYAK", "AUV", "ROV", "ROBOT", "DINGY")
    verbs = ("ran", "crashed", "failed", "befuddled", "wigged out")
    adv = ("crazily.", "swimmingly.", "foolishly.", "strangely.", "spectacularly.")
    types = ("success", "warning", "danger", "info")

    message = 'The ' + adj[randrange(0, 5)] + ' ' + nouns[randrange(0, 5)] + ' ' + verbs[randrange(0, 5)] + ' ' + adv[
        randrange(0, 5)]

    next_message_delay = randrange(1, 30)
    return types[randrange(0, 4)], message, next_message_delay