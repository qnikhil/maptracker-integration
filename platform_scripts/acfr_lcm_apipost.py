######################################################################
# This file handles all the platform pose updates.
# 
# An LCM thread listens for incoming messages and they are forwarded
# to Maptracker as HTTP POST messages.
# 
#
######################################################################

import time
import threading
import sys
import math
import select
import os
import collections
import requests
import lcm


LCMROOT ='/home/auv/git/acfr-lcm'
sys.path.append('{0}/build/lib/python{1}.{2}/dist-packages/perls/lcmtypes/'.format(LCMROOT, sys.version_info[0], sys.version_info[1]))


from acfrlcm import auv_status_short_t, ship_status_t
from senlcm import usbl_fix_t, uvc_osi_t
from acfrlcm.auv_global_planner_t import auv_global_planner_t
from platform_tools.platformdata import APIPlatform, get_args, get_validated


# --------------------------------------LCM-THREAD------------------------------------- #

class LcmThread(threading.Thread, APIPlatform):

    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        self.lc = lcm.LCM()
        self.exitFlag = False
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit
        APIPlatform.__init__(self, **kwargs)

    def usblFixHandler(self, channel, data):
        msg = usbl_fix_t.decode(data)

        platform_data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
            },
            
            # required: lat, lon
            # optional, but required for dash display: heading, roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                "lat": round(msg.latitude, 8),                                             # float, decimal degrees
                "lon": round(msg.longitude, 8),                                             # float, decimal degrees
                "depth": round(msg.depth, 1),                           # float, m
                "uncertainty": round(msg.accuracy, 2)                   # float, m
            },

            # All stat key-value pairs are optional. For no stats, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
                'XYZ': "{}, {}, {}".format(round(msg.target_x, 1), round(msg.target_y, 1), round(msg.target_z, 1))
            }
        }

        # Use the LCM Channel name as the platform_key so can tell which vehicle the status came from
        # This should match the url field in the config json file for the vehicle
        platform_key = str(channel)

        # And POST the data 
        #post_platformdata(platform_key, platform_data, data_type="platform")
        self.update_platform_data(platform_key, platform_data)

    def shipStatusHandler(self, channel, data):
        msg = ship_status_t.decode(data)

        lat = round(msg.latitude, 8)
        lon = round(msg.longitude, 8)

        platform_data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
            },

            # required: lat, lon
            # optional, but required for dash display: heading, roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                "lat": lat,    						# float, decimal degrees
                "lon": lon,     					# float, decimal degrees
                "heading": round(math.degrees(msg.heading), 2),  	# float, degrees, range: 0:360
                "pitch": round(math.degrees(msg.pitch), 2),      	# float, degrees, range: -180:180
                "roll": round(math.degrees(msg.roll), 2)         	# float, degrees, range: -180:180
            },

            # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
            }

        }

        # Use the LCM Channel name as the platform_key so can tell which vehicle the status came from
        # This should match the url field in the config json file for the vehicle
        platform_key = str(channel)

        # And POST the data 
        #post_platformdata(platform_key, platform_data, data_type="platform")
        self.update_platform_data(platform_key, platform_data)

    def auvStatusHandler(self, channel, data):
        msg = auv_status_short_t.decode(data)
        platform = channel #'auv{}'.format(msg.target_id)
        
        lat = round(msg.latitude, 8)
        lon = round(msg.longitude, 8)
        
        # range checking - TODO confirm range of other values is ok
        alt = float(msg.altitude)/10.0
        #if alt < 0.0:
        #    alt = 99999.99
        head = (float(msg.heading)*2.0) % 360.0 # sent as 2 degree increments range -180..180 to fit in a signed char
        if head < 0:
            head = head + 360.0
        

        platform_data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
                "dvl":  bool(msg.status & (1 << 0)),
                "dvl_bl":  bool(msg.status & (1 << 1)),
                "gps":  bool(msg.status & (1 << 2)),
                "depth":  bool(msg.status & (1 << 3)),
                "comp":  bool(msg.status & (1 << 4)),
                "imu":  bool(msg.status & (1 << 5)),
                "oas":  bool(msg.status & (1 << 6)),
                "nav":  bool(msg.status & (1 << 7)),
                "epuck":  bool(msg.status & (1 << 8)),
                "abort":  bool(msg.status & (1 << 9)),
                "press":  bool(msg.status & (1 << 13)),
                "leak":  bool(msg.status & (1 << 15))
            },
    
            # required: lat, lon
            # optional, but required for dash display: heading, roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                "lat": lat,     					# float, decimal degrees
                "lon": lon,     					# float, decimal degrees
                "alt": alt,                                             # float, m
                "depth": float(msg.depth)/10.0,                       	# float, m [sent as 10*m]
                "heading": head, # float(msg.heading)*2.0,                    	# float, degrees, range: 0:360 [sent as 1/2 degrees 0..360]
                "pitch": float(msg.pitch)/4.0,                     	# float, degrees, range: -180:180 [sent as 1/4*degrees] 
                "roll": float(msg.roll)/4.0                      	# float, degrees, range: -180:180 [sent as 1/4*degrees] 
            },
    
            # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
                "bat": int(msg.charge),                               # int, %, range: 0:100
                "waypt": msg.waypoint,
                "#imgs": msg.img_count
            }
        }
        # # TODO # Over Pitch Alert
        # if msg.pitch == 127:
        #     pass
        # # TODO # Under Pitch Alert
        # if msg.pitch == -127:
        #     pass
        # # Alert if AUV > 6 m TODO - implement this as an alert
        # if data['pose']['depth'] < 6:
        #    data['alert']['DEP<6'] = 1

        # Use the LCM Channel name as the platform_key so can tell which vehicle the status came from
        # This should match the url field in the config json file for the vehicle
        platform_key = str(channel)

        # And POST the data 
        # post_platformdata(platform_key, platform_data, "platform")
        self.update_platform_data(platform_key, platform_data)

    def run(self):
        self.lc.subscribe("AUVSTAT.*", self.auvStatusHandler)
        self.lc.subscribe("SHIP_STATUS.*", self.shipStatusHandler)
        self.lc.subscribe("USBL_FIX.*", self.usblFixHandler)

        timeout = 1  # amount of time to wait, in seconds for subscriptions

        # Main loop just handling incoming LCM messages (unless self destruct)
        while not self.exitFlag:
            rfds, wfds, efds = select.select([self.lc.fileno()], [], [], timeout)
            if rfds:
                self.lc.handle()

    def execute_command(self, platform, args):
        cmd = get_validated(args, 'command', val_type=basestring, val_inlist=["goto", "abort"])
        lc = lcm.LCM()
        if platform == "SIRIUS":
            if cmd == "goto":
                msg = auv_global_planner_t()
                msg.point2_x = get_validated(args, 'lon', val_convert=float, val_type=float, val_min=-90, val_max=90, required=True)  # float(args['lon'])
                msg.point2_y = get_validated(args, 'lat', val_convert=float, val_type=float, val_min=-180, val_max=180, required=True)  # float(args['lat'])
                msg.point2_z = get_validated(args, 'depth', val_convert=float, val_type=float, required=True)  # float(args['depth'])
                msg.point2_att[2] = float(-98765)
                msg.velocity[0] = get_validated(args, 'velocity', val_convert=float, val_type=float, val_min=0, required=True)  # float(args['velocity'])
                msg.timeout = get_validated(args, 'timeout', val_convert=float, val_type=float, val_min=0, required=True)  #float(args['timeout'])
                msg.command = auv_global_planner_t.GOTO
            elif cmd == "abort":
                msg = auv_global_planner_t()
                msg.command = auv_global_planner_t.ABORT
                msg.str = "MANUAL"
                # lc.publish('TASK_PLANNER_COMMAND.{}'.format(platform), msg.encode())
            lc.publish('TASK_PLANNER_COMMAND.{}'.format(platform), msg.encode())
            return "The 'TASK_PLANNER_COMMAND.{}' LCM message has been published".format(platform)
        else:
            print "*** NOT IMPLEMENTED:", platform, args
            raise NotImplementedError("Command interpreter not implemented for: "+platform)


# ----------------------------------END-LCM-THREAD------------------------------------- #


# --------------------------------------MAIN------------------------------------------- #

def Main():
    # get optional args
    args = get_args()

    # instantiate object
    acfrplatforms = LcmThread(maptracker_url=args["url"], maptracker_port=args["port"])

    # start threads
    acfrplatforms.start()

    # keep alive
    acfrplatforms.serve_forever()

# ----------------------------------END-MAIN------------------------------------------- #

if __name__ == "__main__":
    Main()


