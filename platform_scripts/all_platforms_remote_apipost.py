
from platform_tools.platformdata import post_data
from time import sleep
import requests

settings = {
    "remote_url": "http://144.6.225.82",
    "remote_port": 80,
    "post_delay": 5,                        # seconds
    "data": [
        # platforms
        {"type": "platform", "key": "test", "url": "http://localhost:8080/data/platformdata.api.Platform/test"},
        # {"type": "platform", "key": "        soi.fkrib", "url": "http://localhost:8080/data/platformdata.api.Platform/soi.fkrib"},
        {"type": "platform", "key": "AUVSTAT.SIRIUS", "url": "http://localhost:8080/data/platformdata.api.Platform/AUVSTAT.SIRIUS"},
        # {"type": "platform", "key": "USBL_FIX.SIRIUS", "url": "http://localhost:8080/data/platformdata.api.Platform/USBL_FIX.SIRIUS"},
        {"type": "platform", "key": "AUVSTAT.HOLT", "url": "http://localhost:8080/data/platformdata.api.Platform/AUVSTAT.HOLT"},
        # {"type": "platform", "key": "USBL_FIX.HOLT", "url": "http://localhost:8080/data/platformdata.api.Platform/USBL_FIX.HOLT"},
        {"type": "platform", "key": "AUVSTAT.NGA", "url": "http://localhost:8080/data/platformdata.api.Platform/AUVSTAT.NGA"},
        {"type": "platform", "key": "AUVSTAT.WAMV", "url": "http://localhost:8080/data/platformdata.api.Platform/AUVSTAT.WAMV"},
        {"type": "platform", "key": "SHIP_STATUS.ORANGEBOX", "url": "http://localhost:8080/data/platformdata.api.Platform/SHIP_STATUS.ORANGEBOX"},
        {"type": "platform", "key": "uri.lfloat", "url": "http://localhost:8080/data/platformdata.api.Platform/uri.lfloat"},
        {"type": "platform", "key": "whoi.slocum", "url": "http://localhost:8080/data/platformdata.api.Platform/whoi.slocum"},
        {"type": "platform", "key": "umich.imorb", "url": "http://localhost:8080/data/platformdata.api.Platform/umich.imorb"},
        {"type": "platform", "key": "umich.iver", "url": "http://localhost:8080/data/platformdata.api.Platform/umich.iver"},

        # ship
        {"type": "platform", "key": "SHIP_STATUS.FALKOR", "url": "http://localhost:8080/data/platformdata.api.Platform/SHIP_STATUS.FALKOR"},
        {"type": "platform", "key": "SHIP_STATUS.FALKOR", "url": "http://localhost:8080/data/platformdata.demo.Platform/SHIP1"},  # testing

        # missions
        {"type": "mission", "key": "test", "url": "http://localhost:8080/get_mission/platformdata.api.Platform/test"}
    ]
}





target_data = {}

while True:
    for t in settings['data']:
        try:
            r = requests.get(t["url"])
            data = r.json()
            cache_key = t["type"]+"-"+t["key"]
            if cache_key in target_data:
                do_post = data["msgts"] > target_data[cache_key]
            else:
                do_post = True
            if do_post:
                post_data(t["key"], data, data_type=t["type"], maptracker_url=settings["remote_url"], maptracker_port=settings["remote_port"])
                target_data[cache_key] = data["msgts"]
        except Exception as e:
            print "*** ERROR: {} ({})! - {}".format(t["key"], t["type"], e)
    sleep(settings["post_delay"])